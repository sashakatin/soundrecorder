import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import controlP5.*; 
import ddf.minim.*; 
import ddf.minim.ugens.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class soundRecorder extends PApplet {





Minim              minim;
MultiChannelBuffer sampleBuffer;
AudioInput         in;
AudioOutput        output;
Sampler            sampler;

AudioRecorder recorder;
AudioPlayer   song;

ControlP5 controlP5;
CheckBox checkbox;

int countname,
name           = 0,
timeBuffer     = 5000, //UI
numberOfLoops  = 5, //UI
savedTime,
sound_threshold= 20,
sliderValue    = 5,
pan            = 1,
gain           = 3,
balance        = 0; //UI, value in secs

boolean timerOn         = false,
        tooLoud         = false,
        loopMode        = false, //UI
        manualRecording = false; //UI

public void newFile() {      
 countname = (name + 1);
 recorder  = minim.createRecorder(in, "file/sound" + countname + ".wav", true);
}



public void setup() {
 size(640, 480);

 controlP5 = new ControlP5(this);
 checkbox = controlP5.addCheckBox("checkBox", 20, 20);
 // make adjustments to the layout of a checkbox.
 checkbox.setColorForeground(color(120));
 checkbox.setColorActive(color(255));
 checkbox.setColorLabel(color(128));
 checkbox.setItemsPerRow(3);
 checkbox.setSpacingColumn(150);
 checkbox.setSpacingRow(60);
 // add items to a checkbox.
 checkbox.addItem("Manual_rec",0);
 checkbox.addItem("Loop_mode",10);
 
 controlP5.addSlider("numOfSecs", 3, 20, sliderValue, 40, 180, 170, 20);
 controlP5.addSlider("numOfLoops", 3, 20, sliderValue, 40, 250, 170, 20);
 controlP5.addSlider("soundThreshold", 14, 40, 20, 40, 320, 170, 20);
 
 controlP5.addSlider("setPan", -1, 1, 0.5f, 340, 180, 170, 20);
 controlP5.addSlider("setGain", 1, 6, 3, 340, 250, 170, 20);
 controlP5.addSlider("setBalance", 0, 1, 0, 340, 320, 170, 20);
 
 minim = new Minim(this);
 output = minim.getLineOut();
 in = minim.getLineIn(Minim.STEREO, 2048);
 
 newFile();
 textFont(createFont("SanSerif", 16));
 
}

public void draw() {
 background(0); 
 stroke(255);
 
 for ( int i = 0; i < in.bufferSize() - 1; i++ ) {
   line(i, 50 + in.left.get(i)*50, i+1, 50 + in.left.get(i+1)*50);
   line(i, 150 + in.right.get(i)*50, i+1, 150 + in.right.get(i+1)*50);
 }
 
 float median = (in.left.get( in.bufferSize()-1 ) + in.right.get( in.bufferSize()-1 ))*25;
 if ( median > sound_threshold && !tooLoud && !manualRecording ) {
     triggerRecording();
     tooLoud = true;
 }
 
 if ( recorder.isRecording() ) {
   text("Currently recording...", 5, 15);
 } else {
   text("Not recording.", 5, 15);
 }
 
 if ( timerOn ) {
   
   int passedTime = millis() - savedTime;
   
   //println(savedTime, passedTime);
   
   if ( passedTime > timeBuffer ) {
     println("Passed three seconds");
     recorder.endRecord();
     timerOn = false;
     tooLoud = false;
     saveFile();
   }
   
 }
 
}

public void keyReleased() {
  
 if ( key == 'r' && manualRecording ) { triggerRecording(); }
 
 if ( key == 's' && manualRecording ) { saveFile(); }
 
 if ( key == 'p' ) {
   println("sample out", countname);
   
   for ( int j = 0; j < countname; j++ ) {
     song = minim.loadFile("file/sound" + (j+1) + ".wav");
     song.setGain(gain);
     song.setPan(pan);
     song.setBalance(balance);
     if ( loopMode ) {
       song.loop(numberOfLoops);
     } else {
       song.play();
     }
     
   }
   
//   sampleBuffer = new MultiChannelBuffer( 1, 1024 );
//   float sampleRate = minim.loadFileIntoBuffer( "file/A08May1.wav", sampleBuffer );
//   
//   if ( sampleRate > 0 ) {
//     
//     int originalBufferSize = sampleBuffer.getBufferSize();
//     sampleBuffer.setBufferSize( originalBufferSize * 2 );
//     
//     for( int s = 0; s < originalBufferSize; ++s ) {
//       
//      int   delayIndex  = s + int( random( 0, originalBufferSize ) );
//      float sampleValue = sampleBuffer.getSample( 0, s );
//      float destValue   = sampleBuffer.getSample( 0, delayIndex ); 
//      sampleBuffer.setSample( 0, // channel
//                              delayIndex, // sample frame to set
//                              sampleValue + destValue // the value to set
//                            );
//                            
//    }
//    
//    sampler = new Sampler( sampleBuffer, sampleRate, 1 );
////    println(output);
//    sampler.patch( output );
//    
//   }
//   
//   if ( sampler != null ) {
////     sampler.trigger();
//   }

 }
 
}

public void saveFile() {
  name++;
  recorder.save();
  println("Done saving.");
  println(name);
}

public void triggerRecording() {
  
  if ( recorder.isRecording() ) {
   recorder.endRecord();
  } else {
   newFile();
   recorder.beginRecord();
   savedTime = millis();
   
   if ( !manualRecording ) {
     timerOn = true;
   }
   
  }
  
}

public void stop() {
 
 in.close();
 minim.stop();
 super.stop();
 
}

public void controlEvent(ControlEvent theEvent) {
  if( theEvent.isGroup() ) {
//    println("got an event from " + theEvent.group().name() + "\t");
    int setManRec   = (int)theEvent.group().arrayValue()[0],
        setLoopMode = (int)theEvent.group().arrayValue()[1];;
//    println(setManRec, setLoopMode);
    if ( setManRec == 1 ) {
      manualRecording = true;
    } else {
      manualRecording = false;
    } 
    
    if ( setLoopMode == 1 ) {
      loopMode = true;
    } else {
      loopMode = false;
    } 
    
  }
}

public void numOfSecs(float val) {
  timeBuffer = (int)(val*1000); // set number of seconds to record in automatic mode
//  println(timeBuffer);
}

public void numOfLoops(float val) {
  numberOfLoops = (int)val; // set number of seconds to record in automatic mode
  println(numberOfLoops);
}

public void soundThreshold(float val) {
  sound_threshold = (int)val; // set number of seconds to record in automatic mode
  println(sound_threshold);
}

public void setPan(float val) {
  pan = (int)val; // set number of seconds to record in automatic mode
}

public void setGain(float val) {
  gain = (int)val; // set number of seconds to record in automatic mode
}

public void setBalance(float val) {
  balance = (int)val; // set number of seconds to record in automatic mode
}
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "soundRecorder" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
